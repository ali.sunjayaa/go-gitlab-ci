package contextext_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	contextext "gitlab.com/ali.sunjaya/go-gitlab-ci/context"
)

func TestDetach(t *testing.T) {
	key := &struct{ name string }{name: "key"}
	ctx := context.WithValue(context.Background(), key, 13)
	ctx, cancel := context.WithTimeout(ctx, time.Nanosecond)
	cancel() // cancel ensuring context has been canceled

	select {
	case <-ctx.Done():
	default:
		t.Fatal("expected context to be cancelled")
	}

	ctx = contextext.Detach(ctx)

	select {
	case <-ctx.Done():
		t.Fatal("expected context to be detached from parents cancellation")
	default:
		rawValue := ctx.Value(key)
		n, ok := rawValue.(int)
		require.True(t, ok)
		require.Equal(t, 13, n)
	}
}
